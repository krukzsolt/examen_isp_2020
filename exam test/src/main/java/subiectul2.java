public class ThreadApp extends Thread {
    public ThreadApp(String name) {
        super(name);
    }

    public static void main(String[] args) {
        ThreadApp myThread1 = new ThreadApp("MyThread1");
        ThreadApp myThread2 = new ThreadApp("MyThread2");
        ThreadApp myThread3 = new ThreadApp("MyThread3");


        /**
         * in the case of the run method, each counter counts in turn
         * while waiting a second
         * until the next value is printed
         */

        myThread1.run();
        myThread2.run();
        myThread3.run();
    }

    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println(getName() + " message number is " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " done.");
    }
}
