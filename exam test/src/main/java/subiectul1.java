interface I {
}
//implementare interfata
class B {
    long t;
    D d;
    public void b() {}
}
//asociere
class D{
    int met1(G g, F f){
        return 0;
    }
}
//agregare
class G{
    double met3(){
        return 0;
    }
}
//agregare
class F{
    String n() {
        return "yup";
    }
}
//compozitie
class E{
    D d;
    void met1(){

    };
}
//dependency
class C{
    int metB(B b){
        return 0;
    }
};
